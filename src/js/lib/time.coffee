import moment from 'moment'

class Time
  format = "h:mm:ss a"

  @getCurrentTime: () ->
    moment().unix()

  #Convert alarm time to unix format
  #Add a day if alarm time has passed for the day
  @setAlarmTime: (alarmTime) ->
    now = moment().unix()
    alarm = moment(alarmTime, format)
    if alarm.unix() - now <= 0
      alarm = alarm.add(1, 'days').unix()
    else
      alarm = alarm.unix()
    return alarm

  #Check if alarm should go off
  @checkAlarmTime: (alarmTime) ->
    now = moment().unix()
    remaining = alarmTime - now
    if remaining <= 0
      console.log "Trigger alarm!"
    else
      console.log remaining + " seconds left."

export default Time
