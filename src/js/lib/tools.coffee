extend = (a, b) ->
  for key of b
    if b.hasOwnProperty(key)
      a[key] = b[key]
  a

export {extend}
