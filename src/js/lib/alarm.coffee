import howler from 'howler'
import howlConfig from '../config/howl'
import Time from './time.coffee'
import {extend} from './tools.coffee'

class Alarm
  constructor: (opts) ->
    settings = {
      "name" : "",
      "type" : "Time",
      "value" : Time.setAlarmTime("8:00:00 am"),
      "switch" : "on",
      "repeat" : "off"
      "temporary" : "no"
      "sound" : howlConfig.beep,
      "quiet" : "off"
    }
    extend(settings, opts)
    @sound = new Howl({
      src: [settings.sound],
      loop: true
    })

  # if Time.checkAlarmTime is true, set off alarm sound/etc
  start: =>
    @sound.play()

  # if disable button used, set alarm to off
  stop: =>
    @sound.stop()

  # if snooze used, disable and create countdown timer
  snooze: =>
    console.log('Zzz.')

export default Alarm
