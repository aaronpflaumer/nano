import Time from './time.coffee'
import uuid from 'uuid/v4'

class DataService
  @store = window.localStorage

  @loadIndex: () ->
    index = @store.getItem('index')
    if index then return JSON.parse(index)
    @store.setItem('index', JSON.stringify([]))
    return []

  @save: (data) ->
    id = uuid()
    @store.setItem(id, JSON.stringify(data))
    updateIndex(id, @store)
    return id

  updateIndex = (id, store) ->
    index = JSON.parse(store.getItem('index'))
    index.push(id)
    store.setItem('index', JSON.stringify(index))

export default DataService
