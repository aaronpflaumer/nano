appName = "Nano"

logger = {}

info = (msg) ->
  console.info(appName + "[info]: " + msg)

warn = (msg) ->
  console.warn(appName + "[warn]: " + msg)

error = (msg) ->
  console.error(appName + "[error]: " + msg)

log = (msg) ->
  console.log(appName + "[log]: " + msg)

logger.info = info
logger.warn = warn
logger.error = error
logger.log = log

export {info}
export {warn}
export {error}
export {log}

export default logger
