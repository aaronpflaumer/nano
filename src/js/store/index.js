import Vue from 'vue';
import Vuex from 'vuex';
import * as mutations from './mutations';
import * as getters from './getters';

Vue.use(Vuex);

const state = {
  alarms: []
};

const store = new Vuex.Store({
  state,
  mutations,
  getters
});

export { store }
