# Nano
Alarm clock app that can create quick access widgets in Android OS.

#### Technologies
* [Webpack](https://webpack.js.org/)
* [Vue.js](https://vuejs.org/)
* [Vuex](https://vuex.vuejs.org/en/)
* [Onsen UI](https://onsen.io/)
* [Cordova](https://cordova.apache.org/)
