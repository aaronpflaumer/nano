const webpackConfig = require('./webpack.config.js');

module.exports = function(config) {
  config.set({
    basePath: '.',
    frameworks: ['jasmine'],
    browsers: ['PhantomJS'],
    files: [
      {pattern: 'src/**/*.js', watched: false},
      {pattern: 'test/*_test.js', watched: false},
      {pattern: 'test/**/*_test.js', watched: false}
    ],

    reporters: ['progress', 'coverage'],

    preprocessors: {
      'src/**/*.js': ['webpack', 'coverage'],
      'test/*_test.js': ['webpack'],
      'test/**/*_test.js': ['webpack']
    },

    coverageReporter: {
      type : 'html',
      dir : 'coverage/'
    },

    webpack: webpackConfig,

    webpackMiddleware: {
      stats: 'errors-only'
    }
  });
};
